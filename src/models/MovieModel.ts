export interface MovieModel {
  id: string,
  title: string,
  image?: string;
  fullImage?: string;
  createdDate: string;
  description: string,
  isLiked?: boolean;
}