import { MovieModel } from "./MovieModel";

export interface MovieModelNoId extends Omit<MovieModel, 'id' | 'createdDate'> {
}