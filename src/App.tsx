import "./App.css";
import Footer from "./components/common/footer/Footer";
import Header from "./components/common/header/Header";
import Main from "./pages/main/Main";
import { HashRouter as Router, Route, Routes } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import { useEffect } from "react";
import MovieDetails from "./pages/movie_details/MovieDetails";
import { useDispatch } from "react-redux";
import { AppDispatch } from "./store/store";
import NotFound from "./components/common/not-found/NotFound";
import { getMoviesThunk } from "./store/movies/movies.actions";

function App() {
  const dispatch = useDispatch<AppDispatch>();

  useEffect(() => {
    dispatch(getMoviesThunk());
  }, [dispatch]);

  return (
    <div className="App d-flex flex-column p-2 m-auto min-vh-100">
      <Router>
        <Header />

        <main className="mt-4 mb-4">
          <Routes>
            <Route path="/" element={<Main />} />
            <Route path="/movie/:id" element={<MovieDetails />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </main>

        <Footer />
      </Router>
    </div>
  );
}

export default App;
