import { ThunkAction } from "redux-thunk";
import { getMovies, markMovieAsFavorite } from "../../api/api";
import { likeMovie, setMovies } from "./movies.reducer";
import { PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

export const markMovieAsFavoriteThunk =
  (id: string): ThunkAction<void, RootState, unknown, PayloadAction<string>> =>
  async (dispatch) => {
    try {
      await markMovieAsFavorite(id);
      dispatch(likeMovie(id));
    } catch (error) {
      console.error("Failed to like movie:", error);
    }
  };

export const getMoviesThunk =
  (): ThunkAction<void, RootState, unknown, PayloadAction<unknown>> =>
  async (dispatch) => {
    try {
      const movies = await getMovies();
      dispatch(setMovies(movies));
    } catch (error) {
      console.error("Failed to get movies:", error);
    }
  };
