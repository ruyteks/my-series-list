import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { v4 as uuidv4 } from "uuid";
import { MovieModelNoId } from "../../models/MovieModelNoId";
import { moviesAdapter } from "./movies.adapters";
import { initialState } from "./movies.state";

export const moviesSlice = createSlice({
  name: "movies",
  initialState,
  reducers: {
    addMovie: (state, action: PayloadAction<MovieModelNoId>) => {
      moviesAdapter.addOne(state, {
        ...action.payload,
        createdDate: new Date().toISOString(),
        id: uuidv4(),
      });
    },
    setMovies: moviesAdapter.setAll,
    removeMovie: moviesAdapter.removeOne,
    likeMovie: (state, action: PayloadAction<string>) => {
      // TODO: Now it works only to set true
      const movie = state.entities[action.payload];
      if (movie) {
        movie.isLiked = true;
      }
    },
  },
});

export const { addMovie, removeMovie, setMovies, likeMovie } =
  moviesSlice.actions;

export default moviesSlice.reducer;
