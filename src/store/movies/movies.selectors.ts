import {
  TypedUseSelectorHook,
  useSelector as rawUseSelector,
} from "react-redux";
import { RootState } from "../store";
import { moviesAdapter } from "./movies.adapters";
import { MoviesState } from "./movies.state";

const useSelector: TypedUseSelectorHook<RootState> = rawUseSelector;

const { selectAll: selectAllMovies, selectById: selectMovieById } =
  moviesAdapter.getSelectors((state: { movies: MoviesState }) => state.movies);

export const useSelectorMovies = () =>
  useSelector((state) => selectAllMovies(state));
export const useSelectorIsLoading = () =>
  useSelector((state) => state.movies.isLoading);

export const useSelectorMovieById = (movieId: string) =>
  useSelector((state) => selectMovieById(state, movieId));
