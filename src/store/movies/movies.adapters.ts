import { createEntityAdapter } from "@reduxjs/toolkit";
import { MovieModel } from "../../models/MovieModel";

export const moviesAdapter = createEntityAdapter<MovieModel>();