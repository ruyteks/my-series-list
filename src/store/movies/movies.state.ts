import { EntityState } from "@reduxjs/toolkit";
import { MovieModel } from "../../models/MovieModel";
import { moviesAdapter } from "./movies.adapters";

export interface MoviesState extends EntityState<MovieModel, string> {
  isLoading: boolean;
}

export const initialState: MoviesState &
  ReturnType<typeof moviesAdapter.getInitialState> = {
  ...moviesAdapter.getInitialState(),
  isLoading: false,
};
