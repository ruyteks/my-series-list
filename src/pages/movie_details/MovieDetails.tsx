import { Link, useParams } from "react-router-dom";
import { useSelectorMovieById } from "../../store/movies/movies.selectors";
import NotFound from "../../components/common/not-found/NotFound";
import Image from "react-bootstrap/Image";
import { formatDateToCustomType } from "../../utils/formatDateToCustomType";

const imageUrl =
  "data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22842%22%20height%3D%22250%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20842%20250%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_18d7142847e%20text%20%7B%20fill%3A%23999%3Bfont-weight%3Anormal%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A42pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_18d7142847e%22%3E%3Crect%20width%3D%22842%22%20height%3D%22250%22%20fill%3D%22%23373940%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22313.56640625%22%20y%3D%22143.75%22%3E842x250%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E";

function MovieDetails() {
  let { id } = useParams();
  const movie = useSelectorMovieById(id!);

  if (!movie) {
    return <NotFound />;
  }

  return (
    <section>
      <Link to={'/#' + movie.id} className="btn btn-outline-primary btn-sm mt-2 mb-2">
        Go back
      </Link>

      <div className="d-flex gap-4">
        <div style={{ flex: 'none', width: 250 }}>
          <Image src={movie.fullImage ? movie.fullImage : imageUrl} thumbnail />
        </div>

        <div>
          <h2>{movie.title}</h2>

          <p>{formatDateToCustomType(movie.createdDate)}</p>

          <p>{movie.description}</p>
        </div>
      </div>
    </section>
  );
}

export default MovieDetails;
