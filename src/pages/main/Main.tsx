import MovieCard from "../../components/common/card/MovieCard";
import SearchInput from "../../components/common/search-input/SearchInput";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Spinner from "react-bootstrap/Spinner";

import { useCallback, useEffect, useState } from "react";
import CardCreateFormDialog from "../../components/card_create_form_dialog/CardCreateFormDialog";
import Button from "react-bootstrap/esm/Button";
import DeleteConfirmDialog from "../../components/delete_confirm_dialog/DeleteConfirmDialog";
import { AppDispatch } from "../../store/store";
import { useDispatch } from "react-redux";
import { addMovie, removeMovie } from "../../store/movies/movies.reducer";
import { MovieModelNoId } from "../../models/MovieModelNoId";
import { markMovieAsFavoriteThunk } from "../../store/movies/movies.actions";
import { useSelectorIsLoading, useSelectorMovies } from "../../store/movies/movies.selectors";

function Main() {
  const movies = useSelectorMovies();
  const isLoading = useSelectorIsLoading();
  const dispatch = useDispatch<AppDispatch>();

  const [visibleMovies, setVisibleMovies] = useState(movies);
  const [searchQuery, setSearchQuery] = useState("");
  const [isShowAddCardDialog, setIsShowAddCardDialog] = useState(false);
  const [intentionToDeleteId, setIntentionToDeleteId] = useState<string | null>(
    null
  );

  useEffect(() => {
    setVisibleMovies(
      searchQuery.length === 0
        ? movies
        : movies.filter((item) =>
            item.title.toLowerCase().includes(searchQuery)
          )
    );
  }, [movies, searchQuery]);

  const handleSearch = useCallback((searchQuery: string) => {
    setSearchQuery(searchQuery.toLowerCase());
  }, []);

  const handleCreateCard = useCallback(
    (card: MovieModelNoId) => {
      dispatch(addMovie(card));
      setIsShowAddCardDialog((prev) => !prev);
    },
    [dispatch]
  );

  const handleDeleteCard = useCallback(
    (id: string) => {
      setIntentionToDeleteId(null);
      dispatch(removeMovie(id));
    },
    [dispatch]
  );

  const handleIntentionToDelete = useCallback((id: string) => {
    setIntentionToDeleteId(id);
  }, []);

  const handleToggleCreateDialog = useCallback(() => {
    setIsShowAddCardDialog((prev) => !prev);
  }, []);

  const handleLikeCard = useCallback(
    (id: string) => {
      dispatch(markMovieAsFavoriteThunk(id));
    },
    [dispatch]
  );

  return (
    <>
      <div className="main">
        {isShowAddCardDialog && (
          <CardCreateFormDialog
            onChange={handleCreateCard}
            onClose={handleToggleCreateDialog}
          />
        )}

        {intentionToDeleteId && (
          <DeleteConfirmDialog
            onSubmit={() => handleDeleteCard(intentionToDeleteId)}
            onClose={() => setIntentionToDeleteId(null)}
          />
        )}

        {/* <SearchInput onChange={handleSearch} /> */}

        {/* <Button
          type="button"
          variant="outline-primary"
          onClick={() => handleToggleCreateDialog()}
        >
          + Add new item
        </Button> */}

        <section className="mt-5">
          {isLoading && (
            <div className="d-flex justify-content-center">
              <Spinner animation="border" role="status">
                <span className="visually-hidden">Loading...</span>
              </Spinner>
            </div>
          )}

          {!isLoading && (
            <Row xs={1} sm={2} md={3} lg={4} className="g-4">
              {visibleMovies.map((e) => (
                <Col key={e.id}>
                  <MovieCard
                    movie={e}
                    onDelete={handleIntentionToDelete}
                    onLike={handleLikeCard}
                  />
                </Col>
              ))}
            </Row>
          )}

          {!isLoading && visibleMovies.length === 0 && (
            <div className="fw-bold text-black-50">
              <span>No any results.</span>

              {searchQuery.length > 0 && <span>&nbsp;Change search query</span>}
            </div>
          )}
        </section>
      </div>
    </>
  );
}

export default Main;
