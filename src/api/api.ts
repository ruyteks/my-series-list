import { MovieModel } from "../models/MovieModel";
import { MovieFavoriteRequest, MovieResponse } from "./apiModels";
import { apiRequest } from "../utils/apiRequest";

const API_ACCOUNT_ID = import.meta.env.VITE_API_ACCOUNT_ID;

export const getMovies = async (): Promise<MovieModel[]> => {
  const data = await apiRequest<MovieResponse>(
    "/discover/movie?include_adult=false&include_video=false&language=en-US&page=1&sort_by=popularity.desc"
  );

  return data.results.map((item) => ({
    id: item.id.toString(),
    title: item.title,
    image: `http://image.tmdb.org/t/p/w500/${item.backdrop_path}`,
    fullImage: `http://image.tmdb.org/t/p/w500/${item.poster_path}`,
    createdDate: item.release_date ?? new Date().toISOString(),
    description: item.overview,
  }));
};

export const markMovieAsFavorite = async (id: string) => {
  await apiRequest<void, MovieFavoriteRequest>(
    `/account/${API_ACCOUNT_ID}/favorite`,
    "POST",
    {
      media_type: "movie",
      media_id: id,
      favorite: true,
    }
  );
};
