export type MovieResponse = { results: MovieResponseResult[] };

export type MovieResponseResult = {
  id: number;
  title: string;
  backdrop_path: string;
  poster_path: string;
  release_date: string;
  overview: string;
};

export type MovieFavoriteRequest = {
  media_type: 'movie';
  media_id: string;
  favorite: boolean;
};
