import Modal from 'react-bootstrap/esm/Modal';
import Button from 'react-bootstrap/esm/Button';
import { useCallback, useState } from 'react';
import InputComponent from '../input/input';
import { MovieModelNoId } from '../../models/MovieModelNoId';

interface CardCreateFormDialogProps {
  onChange?: (result: MovieModelNoId) => void,
  onClose: () => void;
}

function CardCreateFormDialog({ onChange, onClose }: CardCreateFormDialogProps) {
  const [formData, setFormData] = useState<MovieModelNoId>({
    title: '',
    description: '',
  });

  const handleInputChange = (field: string) => (value: string) => {
    setFormData({ ...formData, [field]: value });
  };

  const handleSubmit = useCallback((event: React.ChangeEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const values = Object.fromEntries(formData.entries()) as unknown as MovieModelNoId;
    onChange && onChange(values);
    setFormData({
      title: '',
      description: '',
      image: undefined,
    });
  }, [onChange]);

  return (
    <Modal show onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>Add new item</Modal.Title>
      </Modal.Header>

      <form onSubmit={handleSubmit}>
        <Modal.Body>
          <InputComponent
            type='text'
            label='Movie name'
            placeholder='Write your movie name...'
            name='title'
            value={formData.title}
            required
            onChange={handleInputChange('title')}
          />

          <InputComponent
            type='text'
            label='Movie description'
            placeholder='Write your description name...'
            name='description'
            value={formData.description}
            required
            onChange={handleInputChange('description')}
          />

          <InputComponent
            type='text'
            label='Image link'
            placeholder='Put your image link...'
            name='image'
            value={formData.image}
            onChange={handleInputChange('image')}
          />
        </Modal.Body>

        <Modal.Footer>
          <Button variant="outline-secondary" onClick={onClose}>
            Close
          </Button>

          <Button variant="outline-primary" type="submit">
            Add new item
          </Button>
        </Modal.Footer>
      </form>
    </Modal>
  )
}

export default CardCreateFormDialog;
