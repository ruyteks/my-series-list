import Modal from 'react-bootstrap/esm/Modal';
import Button from 'react-bootstrap/esm/Button';

interface DeleteConfirmDialogProps {
  onSubmit: () => void;
  onClose: () => void;
}

function DeleteConfirmDialog({ onSubmit, onClose }: DeleteConfirmDialogProps) {
  return (
    <Modal show onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>Confirm to delete</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        The element will be deleted without possibility to restore it.
      </Modal.Body>

      <Modal.Footer>
        <Button variant="outline-secondary" onClick={onClose}>
          Discard
        </Button>

        <Button variant="outline-primary" type="submit" onClick={onSubmit}>
          Confirm
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default DeleteConfirmDialog;
