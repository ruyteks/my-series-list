import './Footer.css';
import { FaGitlab, FaLinkedin } from "react-icons/fa6";


function Footer() {
  return (
    <footer className='footer'>
      <p className='footer__copyright'>&copy; 2024 developed by Eduard Rudenko</p>

      <div className='footer__links'>
        <a href="https://gitlab.com/ruyteks" target="_blank" rel="noreferrer" className='footer__link'>
          <FaGitlab />
        </a>

        <a href="https://www.linkedin.com/in/eduard-rudenko/" target="_blank" rel="noreferrer" className='footer__link'>
          <FaLinkedin className='footer__link' />
        </a>
      </div>
    </footer>
  );
}

export default Footer;
