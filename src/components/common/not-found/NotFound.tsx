import './NotFound.css';
import { Link } from "react-router-dom";


function NotFound() {
  return (
    <section className="not-found d-flex flex-column justify-content-center align-items-center">
      <h3>404</h3>

      <p>
        NOT FOUND
      </p>

      <Link to="/" className="btn btn-outline-primary btn-sm">
        Go home page
      </Link>
    </section>
  );
}

export default NotFound;
