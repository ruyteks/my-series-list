import InputComponent from '../../input/input';

interface SearchInputProps {
  onChange?: (value: string) => void;
}

function SearchInput({ onChange }: SearchInputProps) {
  return (
    <InputComponent
      type="search"
      placeholder="Search your movies..."
      onChange={onChange}
    />
  );
}

export default SearchInput;
