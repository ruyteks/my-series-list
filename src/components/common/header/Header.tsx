import Nav from "react-bootstrap/esm/Nav";
import "./Header.css";
import { PiTelevisionSimpleBold } from "react-icons/pi";
import { Link } from "react-router-dom";
import DropdownButton from "react-bootstrap/esm/DropdownButton";
import Dropdown from "react-bootstrap/esm/Dropdown";
import Form from "react-bootstrap/esm/Form";
import { useEffect, useState } from "react";

function Header() {
  const [language, setLanguage] = useState<Language>("EN");

  useEffect(() => {
    localStorage.setItem("language", JSON.stringify(language));
  }, [language]);

  return (
    <header className="header pb-2">
      <Link to="/" className="header__logo-link">
        <PiTelevisionSimpleBold size={32} />

        <span className="header__company-name ms-1">Rudekk</span>
      </Link>

      <Nav>
        <Nav.Item>
          <Nav.Link href="/">Popular</Nav.Link>
        </Nav.Item>

        <Nav.Item>
          <Nav.Link href="/favourites">Favourites</Nav.Link>
        </Nav.Item>
      </Nav>

      <div className="header__right-panel">
        <DropdownButton
          className="header__profile-item"
          title="Profile"
          size="sm"
          variant="light"
        >
          <Dropdown.Item href="/profile">Personal info</Dropdown.Item>
          <Dropdown.Item href="/logout">Log out</Dropdown.Item>
        </DropdownButton>

        <Form.Select
          aria-label="Default select example"
          className="header__language-selector"
          value={language}
          size="sm"
          onChange={(event) => setLanguage(event.target.value as Language)}
        >
          <option value="EN">EN</option>
          <option value="RU">RU</option>
        </Form.Select>
      </div>
    </header>
  );
}

type Language = "EN" | "RU";

export default Header;
