import Card from "react-bootstrap/Card";
import { PiHeartDuotone, PiHeartFill } from "react-icons/pi";

import "./MovieCard.css";
import { MovieModel } from "../../../models/MovieModel";
import { Link } from "react-router-dom";

interface MovieCardProps {
  movie: MovieModel;
  onDelete: (id: string) => void;
  onLike: (id: string) => void;
}

const noImageUrl =
  "data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22286%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20286%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_189c7b639a2%20text%20%7B%20fill%3A%23999%3Bfont-weight%3Anormal%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_189c7b639a2%22%3E%3Crect%20width%3D%22286%22%20height%3D%22180%22%20fill%3D%22%23373940%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22107.19140625%22%20y%3D%2296.3%22%3E286x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E";

function MovieCard({ movie, onDelete, onLike }: MovieCardProps) {
  return (
    <Card className="movie-card" id={movie.id}>
      <Card.Img
        variant="top"
        src={movie.image?.length ? movie.image : noImageUrl}
      />

      <Card.Body>
        <Card.Title className="movie-card__title" title={movie.title}>
          {movie.title}
        </Card.Title>

        <Card.Text className="movie-card__text">{movie.description}</Card.Text>

        <button
          className="movie-card__like-button"
          onClick={() => onLike(movie.id)}
        >
          {movie.isLiked ? (
            <PiHeartFill size={14} />
          ) : (
            <PiHeartDuotone size={14} />
          )}
        </button>

        <Link
          to={`/movie/${movie.id}`}
          className="btn btn-outline-primary btn-sm w-100"
        >
          Open movie
        </Link>
      </Card.Body>
    </Card>
  );
}

export default MovieCard;
