import { HTMLInputTypeAttribute, useCallback } from 'react';
import './input.css';
import Form from 'react-bootstrap/Form';

interface InputComponentProps {
  type: HTMLInputTypeAttribute;
  placeholder: string;
  label?: string;
  name?: string;
  value?: string | number;
  required?: boolean;
  onChange?: (value: string) => void;
}

function InputComponent({ type, name, value, placeholder, label, required, onChange }: InputComponentProps) {
  const onChangeCallback = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    onChange && onChange(((event.target as HTMLInputElement).value ?? '').toLowerCase());
  }, [onChange]);

  return (
    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
      {label && <Form.Label>{label}</Form.Label>}
      
      <Form.Control
        type={type}
        placeholder={placeholder}
        name={name}
        value={value}
        required={required}
        onChange={onChangeCallback}
      />
    </Form.Group>
  )
}

export default InputComponent;
