import { v4 as uuidv4 } from 'uuid';
import { MovieModel } from "../models/MovieModel";

const createMovieMock = (card?: Partial<MovieModel>) => {
  return {
    id: uuidv4(),
    title: 'Card title',
    createdDate: new Date().toISOString(),
    description: 'Some quick example text to build on the card title and make up the bulk of the cards content.',
    ...card,
  };
};

export const MOVIES_MOCKS: MovieModel[] = Array
  .from({ length: 50 })
  .map((_, index) => createMovieMock({ title: index % 3 === 0 ? `Card title ${index}` : 'Card title Nemo' }));
