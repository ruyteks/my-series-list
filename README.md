# My Movies List
Application with connection to TMDB API, that allowing you to search for popular movies, add/delete to favourite, personal estimation of movie.

# Plan of work
- Implement add to favourite list - Done
- Implement menu
- Implement favourite list page - In progress
- Implement highlighting items in top list, that were added to favouries - Planned
- Implement search - Planned
- Removing favourite - Planned
- Implement detailed page - Planned
- Implement personal estimation - Planned
- Implement languages - Planned
- Implement paginatior - Planned
- Implement filtering - Planned

## Tech stack
- React 18
- TypeScript
- Redux store management + Redux toolkit + Redux thunk
- React bootstrap
- Cypress E2E- Planned
- Jest Unit-tests - Planned
- SCSS - Planned

## Install
- `yarn install`

## Run and watch
- `yarn start` - will run React project